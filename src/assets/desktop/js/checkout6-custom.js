// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.
function hasRecurrence() {
    var width = $(window).width();
    console.log(width);

    var buttonFinalizar = $('#cart-to-orderform')

    if (width >= 800) {
        $('.cart-links-bottom').prepend('<label class="x-recorrencia-label" style="position:absolute; top: 0; text-align: center;line-height: 40px;font-size: 14px;width: 100%;text-decoration: underline;background: white;"><input id="recurrence-termos" style="margin:0;" type="checkbox" name="" value="aceito"/> Li, aceito e concordo com os termos de assinatura</label>');
    } else {
        $('.cart-links-bottom').prepend('<label class="x-recorrencia-label" style="position:absolute; bottom: 0; text-align: center;line-height: 40px;font-size: 12px;width: 95%;text-decoration: underline;background: white;border: 1px solid rgb(183,183,183);border-top: none;"><input id="recurrence-termos" style="margin:0;" type="checkbox" name="" value="aceito"/> Li, aceito e concordo com os termos de assinatura</label>');
    }

    $(buttonFinalizar).css('pointer-events', 'none');
    $(buttonFinalizar).css('opacity', '0.2');

    $('body').on('click', '#recurrence-termos', function (event) {
        if ($(this).is(':checked')) {
            $(buttonFinalizar).css('pointer-events', 'initial');
            $(buttonFinalizar).css('opacity', '1');
            $('.x-assinatura-popup-wrapper').fadeIn();
        }
        else {
            $(buttonFinalizar).css('pointer-events', 'none');
            $(buttonFinalizar).css('opacity', '0.2');
        };
    });

    $('.x-assinatura-popup-close span').on('click', function () {
        $('.x-assinatura-popup-wrapper').fadeOut();
    });

    $('.x-assinatura-popup-cta').on('click', function () {
        $('.x-assinatura-popup-wrapper').fadeOut();
        $('#cart-to-orderform').click();
    });


    vtex.events.subscribe('dataLayer', function (name, variables) {
        var event = name;

        if (event == "payment") {
            dataLayer.push({ 'event': 'payment' });
            setTimeout(function () {
                $('#payment-group-bankInvoicePaymentGroup').hide();
                $('#payment-group-creditCardPaymentGroup').click();
            }, 1000);
        };
    });

}

function RecurrenceProdCart() {
    var recurrenceProd = false;
    var buttonFinalizar = $('#cart-to-orderform')

    vtexjs.checkout.getOrderForm().done(function (orderForm) {
        if ($('body .item-attachments-name-recorrencia').length) {
            hasRecurrence();
        }

        for (var i = 0; i < orderForm.items.length; i++) {
            if (orderForm.items[i].attachmentOfferings[0] != undefined) {
                if (orderForm.items[i].attachmentOfferings[0].name == "RecorrÃªncia") {
                    var recurrenceProd = true;
                }
            }
        }

        $(document).on('click', '.add-item-attachment', function () {
            if (recurrenceProd == true) {
                hasRecurrence();
            }
        })

    });


    $(document).on('click', '#item-attachment-remove-RecorrÃªncia', function () {
        $('.x-recorrencia-label').remove();
        $(buttonFinalizar).css('pointer-events', 'initial');
        $(buttonFinalizar).css('opacity', '1');

        vtex.events.subscribe('dataLayer', function (name, variables) {
            var event = name;

            if (event == "payment") {
                dataLayer.push({ 'event': 'payment' });
                setTimeout(function () {
                    $('#payment-group-bankInvoicePaymentGroup').show();
                }, 1000);

            };
        });
    })
}

$(function () {
    if (true) {
        $(".summary-template-holder").addClass("is--desktop");
    }
});


$(document).ready(function () {
    RecurrenceProdCart();
    var addIcon = setInterval(function () {
        if ($('.link.link-gift-card svg').html() == undefined) {
            $('.link.link-gift-card').append('<svg xmlns="http://www.w3.org/2000/svg" width="48.419" height="26.578" viewBox="0 0 48.419 26.578"><g id="noun_Gift_1721491" transform="translate(-12.188 -29.245)"><path id="Path_37" data-name="Path 37" d="M48.273,56.446c.3-2-4.141-3.214-6.759-3.762.548,2.226,1.36,3.025,2.842,5.095C45.675,59.64,47.732,57.164,48.273,56.446Z" transform="translate(-10.55 -8.432)" fill="#42bec1"></path><path id="Path_38" data-name="Path 38" d="M32.022,52.684c-2.619.548-7.071,1.766-6.759,3.762.541.717,2.6,3.194,3.911,1.333C30.655,55.709,31.467,54.91,32.022,52.684Z" transform="translate(-4.698 -8.432)" fill="#42bec1"></path><path id="Path_39" data-name="Path 39" d="M41.514,43.535c2.619-.541,7.064-1.759,6.759-3.755-.541-.717-2.6-3.194-3.918-1.34C42.874,40.517,42.062,41.316,41.514,43.535Z" transform="translate(-10.55 -3.065)" fill="#42bec1"></path><path id="Path_40" data-name="Path 40" d="M25.262,39.78c-.311,2,4.141,3.214,6.759,3.755-.555-2.219-1.367-3.018-2.849-5.095C27.861,36.586,25.8,39.062,25.262,39.78Z" transform="translate(-4.698 -3.065)" fill="#42bec1"></path><path id="Path_41" data-name="Path 41" d="M12.188,51.268V63.745H60.607V51.268H36.283c2.531,1.171,4.615,2.957,3.843,5.575-.2.683-4.831,6.915-8.4,1.908l-2.585-3.911-2.591,3.911c-3.559,5-8.187-1.211-8.4-1.908-.771-2.619,1.319-4.4,3.843-5.575Z" transform="translate(0 -7.923)" fill="#42bec1"></path><path id="Path_42" data-name="Path 42" d="M12.188,30.138v10.65h8.674c-1.969-1.157-3.356-2.774-2.706-4.98.2-.69,4.838-6.915,8.4-1.908l2.591,3.9,2.585-3.9c3.559-5,8.194,1.211,8.4,1.908.656,2.206-.731,3.823-2.7,4.98H60.607V29.245H12.188Z" transform="translate(0 0)" fill="#42bec1"></path></g></svg>');
        } else {
            // 	clearInterval(addIcon);
        }
    }, 100);
});

$(document).ajaxStop(function () {
    $('.coupon-fieldset .coupon-fields').hide();
    $('.coupon-fieldset').append('<div class="p-click">Clique aqui para utilizar o cupom</div>');
    $('.p-click').css('width', '100%');
    $('.p-click').css('text-align', 'center');
    $('.p-click').css('font-weight', '300');
    $('.p-click').css('cursor', 'pointer');
    $(".p-click").remove();

    clickCep();
});


function clickCep() {
    $('.p-click').on("click", function () {
        $('.coupon-fieldset .coupon-fields').toggle("hide");
    });
}

addScript("https://chkoutme.vteximg.com.br/arquivos/chkoutme_universal.min.js?v=14545646");
function addScript(src) {
    var s = document.createElement('script');
    s.setAttribute('src', src);
    document.body.appendChild(s);
}
$(document).ajaxStop(function () {
    $('.info').each(function (index, item) {
        if ($(this).text().toLowerCase() == 'total') {
            $(this).addClass('total')
        }
    });

    setTimeout(function () {
        $('.payment-group-item.active').text().indexOf('Boleto') > -1 ? $('.total').addClass('has--boleto') : $('.total').removeClass('has--boleto');
    }, 1000);
});

