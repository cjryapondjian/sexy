const Methods = {
    init() {
        Methods.sliderBrands();
        Methods.thumbSlider();
        Methods.productsSlider();
    },

    sliderBrands() {
        $('.js--brands__list').slick({
            arrows: true,
            dots: false,
            slidesToShow: 5,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true
                }
              }
            ]
        });
    },

    thumbSlider() {
        $("#show > ul.thumbs").slick({
            arrows: true,
            dots: false,
            vertical: true,
            slidesToShow: 5
        });
    },

    productsSlider() {
        $("li[id*='helperComplement']").remove();
        $(".j-shelf > ul").slick({
            arrows: true,
            dots: false,
            slidesToShow: 4
        });
    }
};
  
export default {
    init: Methods.init
};