const Methods = {
    init() {
        Methods.btnQuantity();
        Methods.shareItems();
        Methods.sizeTable();
    },

    btnQuantity() {
        var $btnComprarProduto = $('.j-content__product--infos .j-box-buy .buy-button');
        if( $btnComprarProduto.length ){

            $btnComprarProduto.click( function(){
                var $this = $(this);
                var url   = $this.attr('href');
                if( url.indexOf('qty=1') > 0 ){
                    $this.attr('href', url.replace('qty=1', 'qty='+ parseInt( $('.j-box-buy .box-qtd .qtd').val() ) ) );
                }
            });

            var $recebeQtyForm = $btnComprarProduto.parents('.j-box-buy');
            if( $recebeQtyForm.length ){
                $recebeQtyForm.prepend(
                    '<div class="box-qtd">' +
                    '	<button class="btn btn-menos"> - </button>' +
                    '    <input type="text" class="qtd" value="1" />' +
                    '	<button class="btn btn-mais"> + </button>' +
                    '</div>'
                );
                $(document).on('keypress' , '.j-box-buy .box-qtd .qtd', function(e){
                    var tecla = ( window.event ) ? event.keyCode : e.which;   
                    if( ( tecla > 47 && tecla < 58 ) ){
                        return true;
                    }else{
                        if ( tecla == 8 || tecla == 0 ){
                            return true;
                        }else{
                            return false;
                        }
                    }
                });
                $(document).on('keyup' , '.j-box-buy .box-qtd .qtd', function(e){
                    $('.j-box-buy .box-qtd .qtd').val( $(this).val() );
                });
                $(document).on('blur' , '.j-box-buy .box-qtd .qtd', function(e){
                    var $this = $(this);
                    if( $this.val() === '' || parseInt( $this.val() ) < 1 ){
                        $('.j-box-buy .box-qtd .qtd').val(1);
                    }else{
                        $('.j-box-buy .box-qtd .qtd').val( $this.val() );
                    }
                });
                $(document).on('click', '.j-box-buy .box-qtd .btn', function(){
                    var $this = $(this);
                    var $qtd  = $('.j-box-buy .box-qtd .qtd');
                    var valor = parseInt( $qtd.val() );
                    if( $this.hasClass('btn-mais') ){
                        $qtd.val( valor + 1 );
                    }else if( $this.hasClass('btn-menos') ){
                        if( valor > 1 ){
                            $qtd.val( valor - 1 );
                        }
                    }
                });
            }
        }
    },

    shareItems() {
        $('.j-box-social a').each(function () {
            var linkShare = $(this).attr('href');
            var linkProduto = window.location.href;
            var share = linkShare + linkProduto;

            $(this).attr('href', share);
        });
    },

    sizeTable() {
        var btnSize = $('button.j-box-cta__tabela');
        var overlay = $('.overlay');
        var sizeTable = $('.j-medidas');
        var btnClose = $('.j-medidas__close');

        btnSize.live("click", function() {
            overlay.addClass('is--active');
            sizeTable.addClass('is--active');
        });
        
        btnClose.live("click", function() {
            overlay.removeClass('is--active');
            sizeTable.removeClass('is--active');
        });
        
        overlay.live("click", function() {
            overlay.removeClass('is--active');
            sizeTable.removeClass('is--active');
        });
    }
};

export default {
    init: Methods.init
}