import FilterMobile from './filterMobile';

const Methods = {
    init() {
        FilterMobile.init();
    }
};

export default {
  init: Methods.init
};
