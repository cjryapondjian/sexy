const Methods = {
	init() {
        if ( window.innerWidth <= 768 ) {
            Methods.renderStructure();
            Methods.openFilter();
            Methods.closeFilter();
        }        
    },
    
    renderStructure() {
        const wrapperFilterMobile = document.querySelector('.j-filter-mobile');
        const categoryTitle = document.querySelector('.j-category__title');
        const categoryImage = document.querySelector('.j-banner__small');
        const categoryOrder = document.querySelector('.orderBy');
        
        const wrapper = `
                <div class="j-filter__mobile-left">
                    ${categoryImage.innerHTML}
                </div>
                <div class="j-filter__mobile-right">
                    <p class='j-filter__title'>${categoryTitle.innerHTML}</p>
                    <div class="j-filter__mobile-color">
                        <div class="j-filter__mobile-order">
                            ${categoryOrder.innerHTML}
                        </div>
                        <div class="j-filter__mobile-btn">
                            <span class='j-open-filter--mobile'></span>
                        </div>
                    </div>
                </div>
        `;
        
        wrapperFilterMobile.innerHTML += wrapper;
    },

    openFilter() {
        const btn = document.querySelector('.j-filter__mobile-btn');
        const menu = document.querySelector('.j-category__filter');

        btn.addEventListener('click', () => {
            menu.classList.add('is--active');
        });
    },

    closeFilter() {
        const btn = document.querySelector('.j-category__filter-close');
        const menu = document.querySelector('.j-category__filter');

        btn.addEventListener('click', () => {
            menu.classList.remove('is--active');
        });
    }
};

export default {
	init: Methods.init,
}