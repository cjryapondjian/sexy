const Methods = {

  init() {
  
    Methods.bannerMain();
    Methods.sliderShelfs();
    Methods.sliderMosaico();
  },

  bannerMain() {

    $(".j-banner__slider").slick({
      arrows: false,
      dots: true
    });
  },

  sliderShelfs() {

    $('.j-shelf > ul').slick({
      slidesToShow: 2,
      dots: false,
      arrows: true
    });
  },

  sliderMosaico() {

    $('.j-home__blog--banner').slick({
        slidesToShow: 1,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2500,
        speed: 1000,
        arrows: true
    })
  }
};

export default {
  init: Methods.init
};
