import Accordion from "./accordion";
import Minicart from "./minicart";
import Menu from "./menu";

const Methods = {
  init() {
    Accordion.init();
    Menu.init();
    Minicart.init();
  }
};

export default {
  init: Methods.init
};
