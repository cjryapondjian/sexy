const Methods = {
	init() {
        Methods.accordionFooter();
    },
    
    accordionFooter() {
        const btns = document.querySelectorAll('.j-box--list > h4, .j-footer__departament-links > h3');

        for( let i = 0; i < btns.length; i++ ) {
            btns[i].addEventListener('click', (ev) => {
                let curretElement = ev.currentTarget;
                curretElement.parentNode.classList.toggle('is--active');
            });
        }

    }
};

export default {
	init: Methods.init,
}